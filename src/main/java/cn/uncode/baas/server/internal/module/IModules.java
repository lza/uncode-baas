package cn.uncode.baas.server.internal.module;

import cn.uncode.baas.server.internal.module.acl.IAclModule;
import cn.uncode.baas.server.internal.module.data.IDataModule;
import cn.uncode.baas.server.internal.module.mail.IMailModule;
import cn.uncode.baas.server.internal.module.user.IUserModule;
import cn.uncode.baas.server.internal.module.utils.IUtilsModule;

public interface IModules {
	
	IDataModule getData();
	
	IUserModule getUser();

	IUtilsModule getUtils();
	
	IMailModule getMail();
	
	IAclModule getAcl();
	
}
